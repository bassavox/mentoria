+++
categories = ['Education', 'Linux', 'Open Source']
date: '2022-12-01'
title: 'Git no Android'
description: 'Passo a passo para usar git num ambiente Linux dentro do Android'
tags = ['portuguese']
+++

# Instalação
## Repositório de aplicativo F-Droid
Catálogo de software livre para Android

Settings > Apps > Special app access > Install Unknown apps

Baixar o APK de:
- [F-Droid](https://f-droid.org/en/packages/org.fdroid.fdroid/)

## Emulador de Terminal Termux
Emulador de Terminal com cara de Linux

Instalar usando F-Droid

```bash
termux-setup-storage
pkg install git openssh
pwd
cd storage/shared
mkdir git
git clone https://gitlab.com/seu_nome_de_usuario/mentoria.git
cd mentoria
touch 'animal_preferido_da_cor_preferida'
git config --global user.name "seu_nome_de_usuario"
git config --global user.email "email usado no gitlab"
git commit -am'Alguma mensagem...como Meu Primeiro commit, uhu!!'
git push
```
